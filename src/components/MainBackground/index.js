import React, {useEffect, useState} from "react";
import {data} from "../../data";
import classes from "./style.module.scss";

// section 2
import image02 from '../../assets/images/bg/2.png'
import image03 from '../../assets/images/bg/3.png'
import image04 from '../../assets/images/bg/4.png'
import image06 from '../../assets/images/bg/6.png'

//  left neo
import home from '../../assets/images/left-neo/home.png'
import facebook from '../../assets/images/left-neo/facebook.png'
import gotoId01 from '../../assets/images/left-neo/01.png'
import gotoId02 from '../../assets/images/left-neo/02.png'

// download
import NapThe from '../../assets/images/download/nap-the.png'
import AndroidDownload from '../../assets/images/download/android-download.png'
import IOSDownload from '../../assets/images/download/ios-download.png'
import APKDownload from '../../assets/images/download/APK-download.png'
import GiftCode from '../../assets/images/download/giftcode.png'

// button
import bxhFadeButton from '../../assets/images/button/bxh-fade.png'
import bxhNoFadeButton from '../../assets/images/button/bxh-no-fade.png'
import duaTopFadeButton from '../../assets/images/button/duatop-fade.png'
import duaTopNoFadeButton from '../../assets/images/button/duatop-no-fade.png'
import nextButton from '../../assets/images/button/next.png'

// section 3
import BXH1 from '../../assets/images/bg/BXH1.png'

// rank
import rank1 from '../../assets/images/rank/rank1.png'
import rank2 from '../../assets/images/rank/rank2.png'
import rank3 from '../../assets/images/rank/rank3.png'

// style
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from "react-slick";

import {classList} from "../../utils";

const goToSection = (id) => {
  const section = document.getElementById(id);
  section.scrollIntoView({
    behavior: 'smooth'
  });
}

function NextButton(props) {
  const { onClick } = props;
  return (
    <img className={classes.nextButton} onClick={onClick} src={nextButton} alt={'Nhân vật tiếp theo'}/>
  );
}

function PreButton(props) {
  const { onClick } = props;
  return (
    <img className={classes.preButton} onClick={onClick} src={nextButton} alt={'Nhân vật tiếp theo'}/>
  );
}

const MainBackground = ({ isOpen, handleOpen }) => {
  const [isBxh, setBxh] = useState(false);
  const [isDuaTop, setDuaTop] = useState(false);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    swipeToSlide: true,
    rows: 1,
    nextArrow: <NextButton />,
    prevArrow: <PreButton />
  };

  const handleBxh = () => {
    setBxh(true)
    setDuaTop(false)
  }

  const openPopup = () => {

  }

  const handleDuatop = () => {
    setBxh(false)
    setDuaTop(true)
  }

  const section = (id) => {
    const section = document.getElementById(id);
    const windowHeight = window.innerHeight;
    const elementTop = section.getBoundingClientRect().top;
    const elementVisible = 10;

    if (elementTop < windowHeight - elementVisible) {
      section.classList.add(classes.active);
    } else {
      section.classList.remove(classes.active);
    }
  }

  useEffect(() => {
    setBxh(true)
    setDuaTop(false)
    window.addEventListener("scroll", () => section('section-2'));
    window.addEventListener("scroll", () => section('section-3'));

    return () => {
      window.removeEventListener("scroll", () => section('section-2'));
      window.removeEventListener("scroll", () => section('section-3'));
    }
  }, [])

  return (
    <div>
      <div className={classes.page} id={'page-background'}>
        <div className={classList(classes.section, classes.section1)} id="section-1">
          <div className={classes.container}>
            <img onClick={() => handleOpen()} className={classes.text} src={image06} alt={'Thương Ảnh Thôn Thiên'} />
          </div>
        </div>

        <div className={classList(classes.section, classes.section2)} id="section-2">
          <Slider {...settings}>
            <img src={image02} alt={'Nữ Anh Hùng'}/>
            <img src={image02} alt={'Nữ Anh Hùng'}/>
            <img src={image02} alt={'Nữ Anh Hùng'}/>
            <img src={image02} alt={'Nữ Anh Hùng'}/>
          </Slider>
        </div>

        {/*section 3*/}
        <div className={classList(classes.section,classes.section3)} id="section-3">
            <div className={classes.container}>
              <div className={classes.buttonWrapper}>
                <div style={{ marginRight: '20px'}}>
                  <img onClick={handleBxh} src={isBxh ? bxhNoFadeButton : bxhFadeButton} alt={'Bảng xếp hạng'}/>
                </div>
                <div>
                  <img onClick={handleDuatop} src={isDuaTop ? duaTopNoFadeButton : duaTopFadeButton} alt={'Thể lệ đua top'}/>
                </div>
              </div>
              <div className={classes.bxhWrapper}>
                {
                  isBxh && (
                    <div className={classes.bxhImageWrapper}>
                      <table>
                        <tr>
                          <th>HẠNG</th>
                          <th>MÁY CHỦ</th>
                          <th>TÊN NHÂN VẬT</th>
                          <th>LỰC CHIẾN</th>
                          <th>CẤP</th>
                          <th>VIP</th>
                        </tr>
                        <tr>
                          <td>
                            <img src={rank1} alt={'rank1'}/>
                          </td>
                          <td>S402</td>
                          <td>Việt Hưng</td>
                          <td>571474</td>
                          <td>600</td>
                          <td>11</td>
                        </tr>
                        <tr>
                          <td>
                            <img src={rank2} alt={'rank2'}/>
                          </td>
                          <td>S402</td>
                          <td>Việt Hưng</td>
                          <td>571474</td>
                          <td>600</td>
                          <td>11</td>
                        </tr>
                        <tr>
                          <td>
                            <img src={rank3} alt={'rank3'}/>
                          </td>
                          <td>S402</td>
                          <td>Việt Hưng</td>
                          <td>571474</td>
                          <td>600</td>
                          <td>11</td>
                        </tr>
                        {
                          data && data.map((item, idx) => (
                            <tr>
                              <td>{idx + 4}</td>
                              <td>{item.server}</td>
                              <td>{item.name}</td>
                              <td>{item.mana}</td>
                              <td>{item.class}</td>
                              <td>{item.vip}</td>
                            </tr>
                          ))
                        }
                      </table>
                    </div>
                  )
                }

                {
                  isDuaTop && (
                    <div className={classes.noidungduatopImageWrapper}>
                      <img className={classes.noidungduatop} src={image04} alt={'Nội dung đua top'}/>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
      </div>

      {/*neo bên trái*/}
      <div className={classes.leftNeo} id="left-neo" style={{ width: '80px' }}>
        <ul>
          <li>
            <img  onClick={() => goToSection('section-1')} src={home} alt={"home"}/>
          </li>
          <li>
            <img src={facebook} alt={"facebook"}/>
          </li>
          <li>
            <img onClick={() => goToSection('section-2')} src={gotoId01} alt={"id1"}/>
          </li>
          <li>
            <img onClick={() => goToSection('section-3')} src={gotoId02} alt={"id2"}/>
          </li>
        </ul>
      </div>

      {/*/!*neo bên phải*!/*/}
      <div className={classes.rightNeo} id="right-neo">
        <ul>
          <li>
            <a href="https://www.w3schools.com" target={"_blank"}>
              <div>
                <img src={NapThe} alt={"Nạp thẻ"}/>
              </div>
            </a>
          </li>
          <li>
            <a href="https://www.w3schools.com" target={"_blank"}>
              <img src={IOSDownload} alt={"Tải game trên IOS"}/>
            </a>
          </li>
          <li>
            <a href="https://www.w3schools.com" target={"_blank"}>
              <img src={AndroidDownload} alt={"Tải game trên Android"}/>
            </a>
          </li>
          <li>
            <a href="https://www.w3schools.com" target={"_blank"}>
              <img src={APKDownload} alt={"Tải game APK"}/>
            </a>
          </li>
          <li>
            <a href="https://www.w3schools.com" target={"_blank"}>
              <img src={GiftCode} alt={"Nhận gift code"}/>
            </a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default MainBackground
