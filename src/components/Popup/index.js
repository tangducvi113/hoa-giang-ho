import React from "react";
import ReactDOM from "react-dom";
import classes from './style.module.scss';

const Popup = () => {
  return (
    <div className={classes.popup}>
      <iframe src="https://www.youtube.com/embed/AMdPV1LvIjU" title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen/>
    </div>
  )
}

const Backdrop = ({ close }) => {
  return (
    <>
      <div className={classes.layout} onClick={() => close()}/>
      <div className={classes.cancel} onClick={() => close()}>x</div>
    </>
  )
}

const Modal = ({ close }) => {
  return (
    <>
      {ReactDOM.createPortal(<Backdrop close={close}/>, document.getElementById('backdrop'))}
      {ReactDOM.createPortal(<Popup close={close}/>, document.getElementById('popup'))}
    </>
  )
}

export default Modal
