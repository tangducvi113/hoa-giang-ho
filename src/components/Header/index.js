import React from 'react';
import classes from './style.module.scss';

import downloadIOS from '../../assets/images/download/ios-download.png'
import downloadAndroid from '../../assets/images/download/android-download.png'
import downloadAPK from '../../assets/images/download/APK-download.png'
import giftcode from '../../assets/images/download/giftcode.png'

const Header = () => {
  return (
    <div className={classes.headerContainer}>
      <header className={classes.theHeader}>
        <div>
          <a href={'https://www.youtube.com/watch?v=Ca3q3JF7Whw'} target={'_blank'}>
            <img src={downloadIOS} alt={''}/>
          </a>
        </div>
        <div>
          <a href={'https://www.youtube.com/watch?v=Ca3q3JF7Whw'} target={'_blank'}>
            <img src={downloadAndroid} alt={''}/>
          </a>
        </div>
        <div>
          <a href={'https://www.youtube.com/watch?v=Ca3q3JF7Whw'} target={'_blank'}>
            <img src={downloadAPK} alt={''}/>
          </a>
        </div>
        <div>
          <a href={'https://www.youtube.com/watch?v=Ca3q3JF7Whw'} target={'_blank'}>
            <img src={giftcode} alt={''}/>
          </a>
        </div>
      </header>
    </div>
  )
}

export default Header
