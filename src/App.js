import React, {Fragment, useEffect, useState} from "react";
import MainBackground from "./components/MainBackground";
import Header from "./components/Header";
import Popup from "./components/Popup";
import Modal from "./components/Popup";

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const open = () => {
    setIsOpen(true)
  }
  const close = () => {
    setIsOpen(false)
  }

  useEffect(() => {
    if (isOpen) {
      document.body.classList.add('overlay')
    } else {
      document.body.classList.remove('overlay')
    }
  }, [isOpen])

  return (
    <Fragment>
      <Header/>
      <MainBackground handleOpen={open}/>

      {
        isOpen && <Modal close={close}/>
      }
    </Fragment>
  );
}

export default App;
